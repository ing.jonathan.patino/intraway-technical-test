import { Component, OnInit } from '@angular/core';

import { ComponentService } from './component.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'intraway-app';
  num1:number = 0;
  num2:number = 0;

  result:any={};

  message:string;

  constructor(public service:ComponentService) {}

  ngOnInit(): void {}

  callService():void {
    this.message = "";
    console.log('num1='+this.num1);
    console.log('num2='+this.num2);
    
    this.service.callService(this.num1,this.num2).subscribe((res:any) => {
      console.log(res);
      this.result = res; 
      if(this.result.error !== undefined) {
        this.message = this.result.error.message;
      } else if(this.result.list !== undefined) {
        this.message = this.result.list;
      }

    });
    
  }

}
