import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  private urlHostEnpoints: string = 'http://localhost:8080/intraway/api/v1/fizzbuzz/';

  constructor(private http:HttpClient) { }

  callService(num1:number, num2:number): Observable<any> {
    return this.http.get<any>(this.urlHostEnpoints+num1+'/'+num2);
  };
}
