export class ResponseOk {
    timestamp:string;
    code: string;
    description: string;
    list: string;
}