export class ResponseError {
    timestamp: string;
    status: number;
    error: string;
    exception: string;
    message: string;
    path: string;
}